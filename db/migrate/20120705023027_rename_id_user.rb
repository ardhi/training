class RenameIdUser < ActiveRecord::Migration
  def up
   rename_column :articles, :id_user, :user_id;
  end

  def down
   rename_column :articles, :user_id, :id_user;
  end
end
