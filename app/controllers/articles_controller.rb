class ArticlesController < ApplicationController
before_filter :require_login, :only => [:new, :create, :edit, :update, :delete]
   def list
   end
   def show
     @article = Article.find(params[:id])
     @comment = Comment.new
     @comments = Comment.where("article_id = #{params[:id]}")
   end
   def new
     @article = Article.new
   end
   def create
     @article = Article.create(params[:article]) #[:title] tambahin disebelah [:article] supaya dapat mengambil satu per satu
     if @article.save
       flash[:notice] = 'Success save into database'
       redirect_to articles_path
         else
           flash[:error] = 'Not Success please try again'
           render :action => :new
         end
   end
   def edit
    @article = Article.find(params[:id])
   end
   def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      flash[:notice] = 'Success update and save into database'
      redirect_to articles_path
        else
          flash[:error] = 'Not Success to update please try again'
          render :action => :edit
        end
   end
   def destroy
     @article = Article.find(params[:id])
     @article.destroy
     flash[:notice] = 'Success delete from database'
     redirect_to articles_path
   end
end
