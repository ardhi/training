class Product < ActiveRecord::Base
  has_many :categories, :through => :categories_product
  has_many :categories_product
end
