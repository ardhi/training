class Article < ActiveRecord::Base
  attr_accessible :id, :title, :content, :user_id
  belongs_to :user
  has_many :comments, :dependent => :destroy

  validates :title, :length => {:minimum => 3, :maximum => 50},
                    :presence => true,
                    :format => {:with => /[a-zA-Z\s]+$/},
                    :uniqueness => true
  
  validates :content, :presence => true,
                      :length => {:minimum => 5, :maximum => 255}
  
  validates :user_id, :presence => true,
                      :numericality => true
  
  validate :valid_title
  def valid_title
    self.errors[:title] << "don't same with label 'title'" if title == 'title'
  end

  scope :ardhi_articles, where("user_id = 1")
  #scope :Product.price_more_than_1000
  #scope :Article.rating_is_and_above(rating)

end
